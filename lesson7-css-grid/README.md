# CSS Grid
## Properties
* display: grid
* grid-template-rows, grid-template-columns, grid-template
* grid-row-gap, grid-column-gap, grid-gap
* grid-area, grid-column-start & grid-column-end, grid-row-start & grid-column-end
* grid-template-areas

## Practicing tasks
* practice in [tasks](practice/index.html)
* replace flexbox with grid layout in _Portfolio_
* markup _Reviews_ block with Grid

## Reference
* [Grid Garden](https://codepip.com/games/grid-garden/) game for practicing CSS Grid
