const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];
let allClients = [...clients1, ...clients2];
const temp = {};
for (const client of allClients) {
    temp[client] = 1;
}
console.log(temp);
allClients = Object.keys(temp);
console.log(...allClients);