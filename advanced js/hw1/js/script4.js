const satoshi2020 = {
        name: 'Nick',
        surname: 'Sabo',
        age: 51,
        country: 'Japan',
        birth: '1979-08-21',
        location: {
          lat: 38.869422,
          lng: 139.876632
        }
      }
    
      const satoshi2019 = {
        name: 'Dorian',
        surname: 'Nakamoto',
        age: 44,
        hidden: true,
        country: 'USA',
        wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
        browser: 'Chrome'
      }
    
      const satoshi2018 = {
        name: 'Satoshi',
        surname: 'Nakamoto',
        technology: 'Bitcoin',
        country: 'Japan',
        browser: 'Tor',
        birth: '1975-04-05'
      }
    
    
    
      const fullProfile = {
        name: satoshi2020.name || satoshi2019.name || satoshi2018.name,
        surname: satoshi2020.surname || satoshi2019.surname || satoshi2018.surname,
        age: satoshi2020.age || satoshi2019.age || satoshi2018.age,
        country: satoshi2020.country || satoshi2019.country || satoshi2018.country,
        hidden: satoshi2020.hidden || satoshi2019.hidden || satoshi2018.hidden,
        browser: satoshi2020.browser || satoshi2019.browser || satoshi2018.browser,
        birth: satoshi2020.birth || satoshi2019.birth || satoshi2018.birth,
        wallet: satoshi2020.wallet || satoshi2019.wallet || satoshi2018.wallet,
        location: satoshi2020.location || satoshi2019.location || satoshi2018.location,
        technology: satoshi2020.technology || satoshi2019.technology || satoshi2018.technology
      };
      console.log(fullProfile);