const gulp = require('gulp');
const concat = require('gulp-concat');
const del = require('delete');
const uglify = require('gulp-uglify');
const htmlmin = require('gulp-htmlmin');
const autoprefixer = require('gulp-autoprefixer');
const livereload = require('gulp-livereload');

gulp.task('buildCSS', function () {
    return gulp.src('src/css/*.css')
        .pipe(concat('main.css'))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(gulp.dest('dist'))
        .pipe(livereload());

});

gulp.task('buildHTML', function () {
    return gulp.src('src/*.html')
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest('dist'))
        .pipe(livereload());
})


gulp.task('buildJs', function () {
    return gulp.src('src/js/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('dist'))
        .pipe(livereload());
})

gulp.task('clean', function () {
    return del('dist/**', { force: true });
});

gulp.task('default', gulp.series([
    'clean',
    gulp.parallel([
        'buildCSS',
        'buildHTML',
        'buildJs'
    ])
]));

gulp.task('start', function () {
    livereload.listen();
    gulp.watch('src/**/*',
        gulp.series('default')
    )
});
gulp.task('directories', function () {
    return gulp.src('*.*', {read: false})
        .pipe(gulp.dest('./css'))
        .pipe(gulp.dest('./img'))
        .pipe(gulp.dest('./img/content'))
        .pipe(gulp.dest('./img/icons'))
        .pipe(gulp.dest('./fonts'))
        .pipe(gulp.dest('./js'));
});
